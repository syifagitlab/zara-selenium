@regression @search @zara

Feature: Search Zara

 Background:

   Given user access page "https://www.zara.com/id/"
   When user click login button on main page
   And user input email zara "yeaayabc@gmail.com"
   And user input password zara "Ayosemangat1"
   And user click login button zara
   Then system display profile zara

  Scenario Outline: Positive Search Zara
    When user click on search button
    Then system display search field
    When user wait search field
    When user input product name zara <ProductName>
    Then system display search result

      Examples:
        | ProductName |
        | "tas"   |
        | "baju" |
        | "celana" |
        | "jaket" |
        | "topi" |

  Scenario Outline: Negative search Zara
    When user click on search button
    Then system display search field
    When user input product name zara <ProductName>
    Then system display not found item

    Examples:
      | ProductName |
      | "fvfbgbf"|
      | "grtgrgr" |
      | "!!?**" |
      | ".,.,,." |
      | "u,.og9" |

  @regression @zara @filtering @woman

  Scenario: Woman Product Filter Zara
    Given user access page "https://www.zara.com/id/"
    When user click on search button
    When user wait search field
    Then system display search field
    When user input product name zara "celana"
    When user wait woman filter zara
    When user click on woman filter
    Then system display search result woman zara

  @child @filtering
  Scenario: Child Product Filter Zara
    Given user access page "https://www.zara.com/id/"
    When user click on search button
    When user wait search field
    Then system display search field
    When user input product name zara "celana"
    When user wait child filter zara
    When user click on child filter
    Then system display search result child zara

  @man @filtering
  Scenario: Man Product Filter Zara
    Given user access page "https://www.zara.com/id/"
    When user click on search button
    When user wait search field
    Then system display search field
    When user input product name zara "celana"
    When user wait man filter zara
    When user click on man filter zara
    Then system display search search result man zara

