@regression @login @zara

Feature: Login Zara

  Scenario: Positive case login Zara

    Given user access page "https://www.zara.com/id/"
    When user click login button on main page
    And user input email zara "yeaayabc@gmail.com"
    And user input password zara "Ayosemangat1"
    And user click login button zara
    Then system display profile zara

  Scenario Outline: Negative case login Zara

    Given user access page "https://www.zara.com/id/"
    When user click login button on main page
    And user input email zara <usernameOrEmail>
    And user input password zara <password>
    And user click login button zara
    Then system display error message zara

    Examples:
      | usernameOrEmail        | password   |
      | "dbfbhytft@gmail.com"  | "rgtrgd"   |
      | "drgegreerd@gmail.com" | "rfefvsc"  |
      | "rgegrrgg@gmail.com"   | "vbgbcdda" |