@regression @cart @delete @zara

Feature: Add and Delete Zara

    Scenario: Add to Cart Zara and Delete
      Given user access page "https://www.zara.com/id/"
      When user click on search button
      When user wait search field
      Then system display search field
      When user input product name zara "celana"
      When user wait for celana
      And user click on celana panjang lebar
      And user click on xxl
      When user click add to cart
      When user click on cart
      When user click on delete item
      Then system display cart empty
