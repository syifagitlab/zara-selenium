package runners;

import io.cucumber.testng.CucumberOptions;

@CucumberOptions(
        plugin = {"json:target/results/zara/cucumber-report.json",  "html:target/results/zara"},
        features = "src/test/resources/features",
        glue = "steps",
        tags = {"@zara"}

)
public class ZaraTestRunner extends BaseTestRunner
{

}
