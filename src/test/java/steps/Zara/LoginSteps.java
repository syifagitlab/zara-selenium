package steps.Zara;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import pageobject.zara.HomePagePO;
import pageobject.zara.LoginPO;
import utilities.ThreadManager;

public class LoginSteps {
    private WebDriver driver = ThreadManager.getDriver();
    private HomePagePO mainPage = new HomePagePO(driver);
    private LoginPO login = new LoginPO(driver);

    @When("user click login button on main page")
    public void user_click_login_button_on_main_page() throws InterruptedException {
        mainPage.ClickOnLoginButton();
    }

    @When("user input email zara {string}")
    public void user_input_email_zara(String username) {
        login.enterUsernameOrEmail(username);
    }

    @When("user input password zara {string}")
    public void user_input_password_zara(String password) {
        login.enterPassword(password);
    }

    @When("user click login button zara")
    public void user_click_login_button_zara() throws InterruptedException {
        login.clickOnLoginpageButton();
    }

    @Then("system display profile zara")
    public void system_display_profile_zara() throws InterruptedException {
        Assert.assertTrue("Profile not appear", login.verifyProfileAppeared());
    }

    @Then("system display error message zara")
    public void system_display_error_message_zara() throws InterruptedException {
       Assert.assertTrue("Error message not appear", login.verifyErrorMessageAppeared());
    }

}
