package steps.Zara;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import pageobject.zara.CartPO;
import pageobject.zara.SearchPagePO;
import utilities.ThreadManager;

public class CartSteps {
    private WebDriver driver = ThreadManager.getDriver();
    private CartPO cart = new CartPO(driver);

    @When("user wait for celana")
    public void user_wait_for_celana() {
        cart.waitTillCelanaIsClickable();
    }

    @When("user click on celana panjang lebar")
    public void user_click_on_celana_panjang_lebar() throws InterruptedException {
        cart.clickOnCelanaPanjangButton();
    }

    @When("user click on xxl")
    public void user_click_on_xxl() throws InterruptedException {
        cart.clickOnXxlButton();
    }

    @When("user click add to cart")
    public void user_click_add_to_cart() throws InterruptedException {
        cart.clickOnAddCartButton();
    }

    @When("user click on cart")
    public void user_click_on_cart() throws InterruptedException {
        cart.clickCartButton();
    }

    @Then("user click on delete item")
    public void user_click_on_delete_item() throws InterruptedException {
        cart.clickOnDeleteitemButton();
    }

    @Then("system display cart empty")
    public void system_display_cart_empty() throws InterruptedException {
        cart.verifyCartIsEmpty();
    }
}
