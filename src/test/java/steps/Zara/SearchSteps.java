package steps.Zara;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import pageobject.zara.SearchPagePO;
import utilities.ThreadManager;


public class SearchSteps {
    private WebDriver driver = ThreadManager.getDriver();
    private SearchPagePO searchPage = new SearchPagePO(driver);
    private SearchPagePO search = new SearchPagePO(driver);

    @When("user click on search button")
    public void user_click_on_search_button() throws InterruptedException {
       searchPage.clickOnSearchButton();
    }

    @When("user wait search field")
    public void user_wait_search_field() {
        searchPage.waitTillElementIsClickable();
    }

    @Then("system display search field")
    public void system_display_search_field() throws InterruptedException {
        Assert.assertTrue("Search field not appear", search.verifySearchFieldAppeared());
    }

    @When("user input product name zara {string}")
    public void user_input_product_name_zara(String productName) {
        search.enterSearch(productName);
    }

    @Then("system display search result")
    public void system_display_search_result() throws InterruptedException {
        Assert.assertTrue("No search result",search.verifySearchResultAppeared());
    }

    @Then("system display not found item")
    public void system_display_not_found_item() throws InterruptedException {
        Assert.assertFalse("Result appear",search.verifyResultNotFound());
    }

    @When("user click on woman filter")
    public void user_click_on_woman_filter() throws InterruptedException {
        searchPage.clickOnWomanButton();
    }

    @Then("system display search result woman zara")
    public void system_display_search_result_woman_zara() throws InterruptedException {
        searchPage.verifyWomanResult();
    }

    @When("user wait woman filter zara")
    public void user_wait_woman_filter_zara() {
        searchPage.waitTillWomanIsClickable();
    }

    @When("user wait child filter zara")
    public void user_wait_child_filter_zara() {
        searchPage.waitTilChildIsClickable();
    }

    @When("user click on child filter")
    public void user_click_on_child_filter() throws InterruptedException {
        searchPage.clickOnChildButton();
    }

    @Then("system display search result child zara")
    public void system_display_search_result_child_zara() throws InterruptedException {
        searchPage.verifyChildResult();
    }

    @When("user wait man filter zara")
    public void user_wait_man_filter_zara() {
        searchPage.waitTillManIsClickable();
    }

    @When("user click on man filter zara")
    public void user_click_on_man_filter_zara() throws InterruptedException {
        searchPage.clickOnManButton();
    }

    @Then("system display search search result man zara")
    public void system_display_search_search_result_man_zara() throws InterruptedException {
        searchPage.verifyManResult();
    }

}
