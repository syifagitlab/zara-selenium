package pageobject.zara;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import utilities.SeleniumHelpers;

import javax.xml.xpath.XPath;

public class CartPO {
    WebDriver driver;
    SeleniumHelpers selenium;
    private Actions actions;

    public CartPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        //This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, 60), this);
    }

    @FindBy(xpath = "//*[@class='media-image__image media__wrapper--media']")
    private WebElement celanaButton;

    @FindBy(xpath = "//*[@class='product-size-info__name'][contains(text(), 'XL')]")
    private WebElement xxlButton;

    @FindBy(xpath = "//*[@class='button product-cart-buttons__button product-cart-buttons__add-to-cart']")
    private WebElement addtocartButton;

    @FindBy(xpath = "//*[contains(text(), 'Keranjang')]")
    private WebElement cartBar ;

    @FindBy(xpath = "//*[@class='button']")
    private WebElement cartButton;

    @FindBy(xpath = "//*[@class='shop-cart-item__delete-icon']")
    private WebElement deleteitemButton;

    @FindBy(id="//*[@id='empty-cart']")
    private WebElement emptycart;

    /**
     * To wait until element is clickable
     */
    public void waitTillCelanaIsClickable() {
        WebDriverWait wait = new WebDriverWait(driver, 1000);
        wait.until(ExpectedConditions.elementToBeClickable(celanaButton));
    }

    /**
     * click on celana panjang lebar button
     *
     * @throws InterruptedException
     */
    public void clickOnCelanaPanjangButton() throws InterruptedException {
        selenium.clickOn(celanaButton);
    }

    /**
     * click on xxl button
     *
     * @throws InterruptedException
     */
    public void clickOnXxlButton() throws InterruptedException {
        selenium.clickOn(xxlButton);
    }

    /**
     * click on add to cart button
     *
     * @throws InterruptedException
     */
    public void clickOnAddCartButton() throws InterruptedException {
        selenium.clickOn(addtocartButton);
    }

//    /**
//     * Mouse hover to element
//     * @param el WebElement object
//     */
//    public void hoverElement(WebElement el)
//    {
//        actions.moveToElement(el).perform();
//    }

    /**
     * Mouse hover to element
     */
    public void hoverToElement() { actions.moveToElement(cartBar);

//        actions.moveToElement(cartBar).perform();
    }

    /**
     * click on cart button
     *
     * @throws InterruptedException
     */
    public void clickCartButton() throws InterruptedException {
        selenium.clickOn(cartButton);
    }

    /**
     * click on delete item button
     *
     * @throws InterruptedException
     */
    public void clickOnDeleteitemButton() throws InterruptedException {
        selenium.clickOn(deleteitemButton);
    }

    /**
     * Verify cart is empty
     */
    public boolean verifyCartIsEmpty() throws InterruptedException{
        return selenium.isElementAppeared(emptycart);
        }
        
}
