package pageobject.zara;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import utilities.SeleniumHelpers;

public class SearchPagePO {
    WebDriver driver;
    SeleniumHelpers selenium;

    public SearchPagePO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        //This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, 60), this);
    }

    @FindBy(xpath = "//*[@class='layout-header-search-bar__link']")
    private WebElement searchButton;

    @FindBy(id = "search-term")
    private WebElement searchTermEditText;

    @FindBy(xpath = "//a[contains(text(),'WANITA')]")
    private WebElement searchResult;

    @FindBy(xpath = "//*[@class='menu-item menu-item--level-1 '][1]")
    private WebElement womanButton;

    @FindBy(xpath = "//*[@class='_facet _subcategory'][contains(text(), 'COLLECTION')]")
    private WebElement newWomanButton;

    @FindBy(xpath = "//*[@class='menu-item menu-item--level-1 '][2]")
    private WebElement childButton;

    @FindBy(xpath = "'//*[@class='_facet _subcategory'][contains(text(), 'COLLECTION')]'")
    private WebElement newChildButton;

    @FindBy(xpath = "//*[@class='_facet'][contains(text(), 'PRIA')]")
    private WebElement manButton;

    @FindBy(xpath = "'//*[@class='_facet _subcategory'][contains(text(), 'COLLECTION')]'")
    private WebElement newManButton;

    /**
     * click on search button
     *
     * @throws InterruptedException
     */
    public void clickOnSearchButton() throws InterruptedException {
        selenium.clickOn(searchButton);
    }

    /**
     * Enter search
     *
     * @param Search
     */
    public void enterSearch(String Search) {
        selenium.enterText(searchTermEditText, Search, true);
    }

    /**
     * Verify search result appear
     */
    public boolean verifySearchResultAppeared() throws InterruptedException {
        return selenium.isElementAppeared(searchResult);
    }

    /**
     * Verify search field appear
     */
    public boolean verifySearchFieldAppeared() throws InterruptedException{
        return selenium.isElementAppeared(searchTermEditText);
    }

    /**
     * Verify not found
     */
    public boolean verifyResultNotFound() throws InterruptedException{
        return selenium.isElementAppeared(searchResult);
    }

    /**
     * To wait until element is clickable
     */
    public void waitTillElementIsClickable() {
    WebDriverWait wait = new WebDriverWait(driver, 1000);
    wait.until(ExpectedConditions.elementToBeClickable(searchTermEditText));
        }

    /**
     * click on woman button
     *
     * @throws InterruptedException
     */
    public void clickOnWomanButton() throws InterruptedException {
        Thread.sleep(3000);
        selenium.clickOn(womanButton);
    }

    /**
     * Verify search result appear
     */
    public boolean verifyWomanResult() throws InterruptedException {
        return selenium.isElementAppeared(newWomanButton);
    }

    /**
     * To wait until element is clickable
     */
    public void waitTillWomanIsClickable() {
        WebDriverWait wait = new WebDriverWait(driver, 120);
        wait.until(ExpectedConditions.elementToBeClickable(womanButton));
    }

    /**
     * To wait until element is clickable
     */
    public void waitTilChildIsClickable() {
        WebDriverWait wait = new WebDriverWait(driver, 120);
        wait.until(ExpectedConditions.elementToBeClickable(childButton));
    }

    /**
     * click on child button
     *
     * @throws InterruptedException
     */
    public void clickOnChildButton() throws InterruptedException {
        selenium.clickOn(childButton);
    }

    /**
     * Verify search result appear
     */
    public Boolean verifyChildResult() throws InterruptedException {
        return selenium.isElementAppeared(newChildButton);
    }

    /**
     * click on man button
     *
     * @throws InterruptedException
     */
    public void clickOnManButton() throws InterruptedException {
        selenium.clickOn(manButton);
    }

    /**
     * To wait until element is clickable
     */
    public void waitTillManIsClickable() {
        WebDriverWait wait = new WebDriverWait(driver, 120);
        wait.until(ExpectedConditions.elementToBeClickable(manButton));
    }

    /**
     * Verify search result appear
     */
    public boolean verifyManResult() {
        return selenium.isElementAppeared(newManButton);
    }

}