package pageobject.zara;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.SeleniumHelpers;

public class LoginPO {
    WebDriver driver;
    SeleniumHelpers selenium;
    public LoginPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        //This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, 60), this);
    }
    @FindBy(xpath = "//*[@class='layout-header-link layout-header-links__desktop-link link']/preceding-sibling::a")
    private WebElement loginButton;

    @FindBy(xpath= "//input[@name='email']")
    private WebElement usernameOrEmailEditText;

    @FindBy(xpath = "//input[@name='password']")
    private WebElement passwordEditText;

    @FindBy(xpath = "//*[@class='button logon-view__form-button']")
    private WebElement loginpageButton;

    @FindBy(xpath = "//*[@class='layout-header-link layout-header-links__desktop-link link']/preceding-sibling::a")
    private WebElement verLogin;

    @FindBy(xpath = "//*[@class='button']")
    private WebElement verErrorMessage;

    /**
     * click on login button
     * @throws InterruptedException
     */

    public void clickOnLoginButton() throws InterruptedException {
        selenium.clickOn(loginButton);
    }
    /**
     * Enter username or email
     * @param usernameOrEmail
     */
    public void enterUsernameOrEmail(String usernameOrEmail) {
        selenium.enterText(usernameOrEmailEditText, usernameOrEmail, true);
    }
    /**
     * Enter password
     * @param password
     */
    public void enterPassword(String password) {
        selenium.enterText(passwordEditText, password, true);
    }

    /**
     * click on login button2
     * @throws InterruptedException
     */

    public void clickOnLoginpageButton() throws InterruptedException {
        selenium.clickOn(loginpageButton);
    }

    /**
     * Verify login profil
     */

    public boolean verifyProfileAppeared() throws InterruptedException {
        return selenium.isElementAppeared(verLogin);
    }

    /**
     * Verify login error message appear
     */
    public boolean verifyErrorMessageAppeared() throws InterruptedException {
        return selenium.isElementAppeared(verErrorMessage);
    }
}