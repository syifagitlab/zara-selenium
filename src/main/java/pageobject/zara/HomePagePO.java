package pageobject.zara;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.SeleniumHelpers;

public class HomePagePO {
    WebDriver driver;
    SeleniumHelpers selenium;

    public HomePagePO(WebDriver driver){
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        //This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, 60), this);
    }

    @FindBy(xpath = "//*[@class='layout-header-link layout-header-links__desktop-link link']/preceding-sibling::a")
    private WebElement homeloginButton;

    /**
     * Click on login button
     */
    public void ClickOnLoginButton() throws InterruptedException {
        selenium.clickOn(homeloginButton);
    }


}
